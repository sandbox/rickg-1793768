jQuery(document).ready(function () {
  jscolor.dir = Drupal.settings.jscolor.dir;
  jscolor.matchInstance = new RegExp('^([a-z0-9_-]+)\\[([a-z0-9_-]+)\\]\\[([a-z0-9_-]+)\\]\\[value\\]$', 'i');
  jscolor.preload();

  jQuery('input.'+Drupal.settings.jscolor.bindClass).each(function() {
    if(!this.color && this.name) {
      var inst = this.name.match(jscolor.matchInstance),
        prop = Drupal.settings.jscolor.props[inst[1]][inst[2]]['d'+inst[3]];

      if (prop.hasOwnProperty('valueElement')) {
        prop.valueElement = (typeof prop.valueElement === 'string') ? jQuery('#'+prop.valueElement).get(0) : this;
      }
      if (prop.hasOwnProperty('styleElement')) {
        prop.styleElement = (typeof prop.styleElement === 'string') ? jQuery('#'+prop.styleElement).get(0) : this;
      }
      if (prop.hasOwnProperty('onImmediateChange')) {
        prop.onImmediateChange = (typeof prop.onImmediateChange === 'string') ? prop.onImmediateChange+'(this);' : null;
      }
      this.color = new jscolor.color(this, prop);
    }
  });

});

function testImmediateChange(color) {
  console.log(color.valueElement.value);
}

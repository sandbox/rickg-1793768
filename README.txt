Drupal jscolor module:
------------------------
Maintainers:
  Rick Gutzmer (http://drupal.org/user/1039432)
Requires - Drupal 7
License - GPL (see LICENSE)


Overview:
--------
JSColor is a simple & user-friendly color picker for your HTML forms.
It extends all desired <input> fields of a color selection dialog.
This module allows for integration of jscolor into Drupal.

The module implements a new widget for field type textfield.  This 
widget supports all the settings of the jscolor javascript object
through the widget settings form.  On display of the formfield input
using this widget, the module loads the jscolor javascript library and
stores the instance settings in Drupal.settings to customize the
JavaScript Color Picker interface.

* jscolor - http://jscolor.com/


Features:
---------

The JSColor module:

* Provides a "jsColor Picker" widget for textfields
* Supports a all the instance-level options allowed by jscolor

The JSColor plugin:

* Provides a simple clickable color palate to select rgb colors
* Appearance is controlled through CSS through runtime settings.
* JSColor is LGPL licensed, so you can use it everywhere, 
  even in commercial applications.


Installation:
------------
1. Download and unpack the jscolor plugin in "sites/all/libraries".
   Link: http://jscolor.com/download.php
   If the Libraries API module (http://drupal.org/project/libraries)
   is installed, you can unpack the plugin into any supported library
   location.
2. Download and unpack the jscolor module directory in your modules
   folder (this will usually be "sites/all/modules/").
3. Go to "Administer" -> "Modules" and enable the module.


Configuration:
-------------
There is no module-level configuration.

To configure a specific instance/bundle, attach a textfield to a 
fieldable entity with the JSColor Picker widget, and edit the 
widget settings.

The most common configuration change from defaults is to turn on
the hash-mark "#" prefix to the color value.

The widget provided by this module is avaiable to any fieldable
entity.
